<?php
require("./conf.inc.php");


function genHeader(){
	printf("#!/bin/bash");
	printf("\n");
}

function genBody(){
	for($i = Config::ID_FROM ;$i<Config::ID_TO ;$i++){
		printf("java -cp %s com.mozat.morange.simulator.MoServiceGroup %s %s 123 1 & \n", Config::LIB_STR, Config::$arr_monet[$i%count(Config::$arr_monet)], sprintf(Config::NAME_TEMPLATE, $i));
	}
}

genHeader();
genBody();