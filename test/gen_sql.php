<?php
require("./conf.inc.php");

function genHeader(){
	return "INSERT INTO [public_domain].[dbo].[user_info]([user_id],[user_type],[user_name],[password],[user_level_expiry_dt]) VALUES ";
}

function genValue($id, $type){	
	return sprintf("(%d,%d,'%s',0x202CB962AC59075B964B07152D234B70,1387190789)", $id, $type, sprintf(Config::NAME_TEMPLATE, $id));
}

function genBatch($idFrom, $idTo, $type){
	$step = 1;
	for($id = $idFrom; $id<$idTo; $id+=$step){		
		printf(genHeader());
		
		$iiend =  min($id+$step, $idTo);
		for($ii = $id; $ii<$iiend; $ii++){			
			printf(genValue($ii, $type));			
			if($ii<$iiend-1){
				printf(",");
			}
		}
		
		printf("\n");		
	}
}

genBatch(Config::ID_FROM, Config::ID_TO,Config::TYPE);

?>
