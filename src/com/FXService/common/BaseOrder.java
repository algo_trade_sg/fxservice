package com.FXService.common;

import java.io.Serializable;
import java.util.Vector;

public class BaseOrder implements Serializable {

    public enum OrderStatus {

        INVALID("INVALID"), WAIT_PLACE_CONFIRM("WAIT_PLACE_CONFIRM"), PLACE_CONFIRM("PLACE_CONFIRM"),
        PARTIAL_FILLED("PARTIAL_FILLED"), FULL_FILLED("FULL_FILLED"), PLACE_REJECT("PLACE_REJECT"),
        WAIT_CANCEL_CONFIRM("WAIT_CANCEL_CONFIRM"), CANCEL_CONFIRM("CANCEL_CONFIRM"), CANCELED("CANCELED"),
        CANCEL_REJECT("CANCEL_REJECT");
        private final String name;

        private OrderStatus(String s) {
            name = s;
        }

        public boolean equals(String otherName) {
            return (otherName == null) ? false : name.equals(otherName);
        }

        public boolean equals(OrderStatus otherName) {
            return (otherName == null) ? false : name.equals(otherName.toString());
        }

        @Override
        public String toString() {
            return name;
        }
    }
    public static final int SIDE_BID = 1;
    public static final int SIDE_SELL = 2;
    public static final int SIDE_ALL = 3;
    public static final int CONTRA_OPEN = 1;
    public static final int CONTRA_CLOSE = 2;
    public static final int TYPE_STOCK = 1;
    public static final int TYPE_BOND = 2;
    public static final int TYPE_CBOND = 3;
    // main order fields
    public String symbol;
    public String currency;
    public String orderId;
    public int side;
    public int totalQuantity;
    public double lmtPrice;
    // extended order fields
    public int remainQuantity;
    public double avgPrice;
    public String ct;
    public OrderStatus status;

    public BaseOrder() {
        symbol = "";
        currency = "";
        orderId = "";
        side = -1;
        totalQuantity = 0;
        lmtPrice = 0;
        remainQuantity = 0;
        avgPrice = 0;
        ct = "";
        status = OrderStatus.INVALID;
    }

    public boolean equals(Object other) {
        if (this == other) {
            return true;
        }
        if (other == null) {
            return false;
        }
        BaseOrder theOther = (BaseOrder) other;
        if (!symbol.equals(theOther.symbol)
                || !currency.equals(theOther.currency)
                || orderId != theOther.orderId
                || side != theOther.side
                || totalQuantity != theOther.totalQuantity
                || lmtPrice != theOther.lmtPrice
                || remainQuantity != theOther.remainQuantity
                || avgPrice != theOther.avgPrice
                || !ct.equals(theOther.ct)
                || status != theOther.status) {
            return false;
        }
        return true;
    }
}
