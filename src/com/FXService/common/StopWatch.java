/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.FXService.common;

import java.util.ArrayList;

/**
 * @author qiangfu
 * @version 0.1
 * @since Mar 1, 2014
 */

public final class StopWatch {

    ArrayList<DescTimePair> list = new ArrayList<DescTimePair>();
    String name;

    public StopWatch() {
        this.reset();
        this.name = "profile";
    }

    public StopWatch(String name) {
        this();
        this.name = name;
    }

    public void reset() {
        list.clear();
        this.tic("");
    }

    public void tic(String desc) {
        list.add(new DescTimePair(desc, System.currentTimeMillis()));
    }

    @Override
    public String toString() {                
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append("[").append(name).append(" ");
        if (this.list.size() >= 2) {                        
            DescTimePair p1 = this.list.get(0), p2;
            for (int k = 1; k < this.list.size(); k++) {
                p2 = this.list.get(k);
                stringBuffer.append(String.format("%s:%d ", p2.descString, p2.time - p1.time));
                p1 = p2;
            }            
        }
        stringBuffer.append(String.format("TOTAL:%d]", System.currentTimeMillis() - this.list.get(0).time));
        return stringBuffer.toString();
    }

    /**
     * @param args
     * @throws InterruptedException
     */
    public static void main(String[] args) throws InterruptedException {
        StopWatch stopWatch = new StopWatch();
        Thread.sleep(500);
        stopWatch.tic("step1");
        Thread.sleep(600);
        stopWatch.tic("step2");
        Thread.sleep(800);
        stopWatch.tic("step3");
        Thread.sleep(1800);
        stopWatch.tic("step4");

        System.out.println(stopWatch);

        stopWatch.reset();
        System.out.println(stopWatch);
    }
}

class DescTimePair {

    public String descString;
    public long time;

    public DescTimePair(String descString, long time) {
        this.descString = descString;
        this.time = time;
    }
}
