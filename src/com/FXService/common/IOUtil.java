/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.common;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

/**
 * @author qiangfu
 * @version 0.1
 * @since Mar 1, 2014
 */

public class IOUtil {

    static public void writeVarchar(DataOutputStream bout, String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        bout.writeByte(bytes.length);
        bout.write(bytes);
    }

    static public String readVarchar(DataInputStream bin) throws IOException {
        int len = bin.readByte();
        if (len <= 0) {
            return "";
        }
        byte[] bytes = new byte[len];
        bin.readFully(bytes, 0, len);
        String ret = new String(bytes, "UTF-8");
        return ret;

    }

    static public void writeShortString(DataOutputStream bout, String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        bout.writeShort(bytes.length);
        bout.write(bytes);
    }

    static public String readShortString(DataInputStream bin) throws IOException {
        int len = bin.readShort();
        if (len <= 0) {
            return "";
        }
        byte[] bytes = new byte[len];
        bin.readFully(bytes, 0, len);
        String ret = new String(bytes, "UTF-8");
        return ret;
    }

    static public void writeString(DataOutputStream bout, String str) throws IOException {
        byte[] bytes = str.getBytes("UTF-8");
        bout.writeInt(bytes.length);
        bout.write(bytes);
    }

    static public String readString(DataInputStream bin) throws IOException {
        int len = bin.readInt();
        if (len <= 0) {
            return "";
        }
        byte[] bytes = new byte[len];
        bin.readFully(bytes, 0, len);
        String ret = new String(bytes, "UTF-8");
        return ret;
    }

    static public void writeShortBytes(DataOutputStream bout, byte[] bytes) throws IOException {
        bout.writeShort(bytes.length);
        bout.write(bytes);
    }

    static public byte[] readUnsignedShortBytes(DataInputStream bin) throws IOException {
        int len = bin.readUnsignedShort();
        if (len <= 0) {
            return null;
        }
        byte[] bytes = new byte[len];
        bin.readFully(bytes, 0, len);
        return bytes;
    }
    
    static public byte[] readShortBytes(DataInputStream bin) throws IOException {
        int len = bin.readShort();
        if (len <= 0) {
            return null;
        }
        byte[] bytes = new byte[len];
        bin.readFully(bytes, 0, len);
        return bytes;
    }

    static public void writeBytes(DataOutputStream bout, byte[] bytes) throws IOException {
        if (bytes == null) {
            bytes = new byte[0];
        }

        bout.writeInt(bytes.length);
        bout.write(bytes);
    }

    static public byte[] readBytes(DataInputStream bin) throws IOException {
        int len = bin.readInt();
        if (len <= 0) {
            return null;
        }
        byte[] bytes = new byte[len];
        bin.readFully(bytes, 0, len);
        return bytes;
    }

    static public void writeRawBytes(DataOutputStream bout, byte[] bytes) throws IOException {
        bout.write(bytes);
    }

    static public byte[] readRawBytes(DataInputStream bin, int len) throws IOException {
        if (len <= 0) {
            return null;
        }
        byte[] bytes = new byte[len];
        bin.readFully(bytes, 0, len);
        return bytes;
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
    }
}
