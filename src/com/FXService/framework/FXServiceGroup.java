package com.FXService.framework;

//import com.FXService.strategy.FXKernelArbitrage;
import java.util.Arrays;
import org.apache.log4j.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

public class FXServiceGroup extends ThreadGroup {

    FXServiceGroup() {
        super("FXKernelServiceGroup");
    }
    static final Logger logger = Logger.getLogger(FXServiceGroup.class);
    public static FXServiceBase FXServiceBase = null;
//    public static FXKernelArbitrage FXKernelArbitrage = null;

    public static void main(String[] args) {
        initFXKernelService();
    }

    private static void initFXKernelService() {
        try {
            FXServiceBase = new FXServiceBase();
            FXServiceBase.startSvr();
            
//            FXKernelArbitrage = new FXKernelArbitrage();
//            FXKernelArbitrage.startSvr();
        } catch (Exception e) {
            logger.error("init FXKernelBase error.", e);
        }
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        logger.error("uncaughtException from thread " + t + " : " + e);
    }
}
