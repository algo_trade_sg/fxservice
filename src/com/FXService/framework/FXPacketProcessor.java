/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.framework;

import com.FXService.framework.Global;
import com.FXService.framework.FXServiceBase;
import com.FXService.util.FXCounter;
//import com.FXService.util.JOrder.OrderStatus;
import com.FXService.util.FXPacket;
import com.FXService.util.FXTag;
import com.FXService.util.FXType;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author qiangfu
 * @version 0.1
 * @since Apr 27, 2014
 */
class JWrongFormatException extends Exception {

    private static final long serialVersionUID = 1L;

    public JWrongFormatException() {
        super();
    }

    public JWrongFormatException(String message) {
        super(message);
    }
}

public class FXPacketProcessor extends Thread {

    private FXServiceBase jServiceBase = null;

    public FXPacketProcessor(FXServiceBase jServiceBase, String name) {
        this.jServiceBase = jServiceBase;
        this.setName(name);
    }

    public void processPkt(FXPacket pkt) throws Exception {
        System.out.println(this.getName() + " got a packet to process!");
        if (pkt.isBroken) {
            throw new JWrongFormatException("Got a broken packet");
        }
        if (pkt.msgType.equals(FXType.DEBUG)) {
            processTestPkt(pkt);
        } else if (pkt.msgType.equals(FXType.HEARTBEAT)) {
            processHeartBeatPkt(pkt);
        } else if (pkt.msgType.equals(FXType.LOGOUT)) {
            processLogoutPkt(pkt);
        } else if (pkt.msgType.equals(FXType.ACCOUNT_REQUEST_RESULT)) {
            processAccountRequestResultPkt(pkt);
        } else if (pkt.msgType.equals(FXType.RESEND_REQUEST)) {
            processResendRequestPkt(pkt);
        } else if (pkt.msgType.equals(FXType.HEARTBEAT_TEST)) {
            processHeartBeatTestPkt(pkt);
        } else if (pkt.msgType.equals(FXType.SESSION_REJECT)) {
            processSessionRejectPkt(pkt);
        } else if (pkt.msgType.equals(FXType.RESET_SEQ_NUMBER)) {
            processResetReqNumberPkt(pkt);
        } else if (pkt.msgType.equals(FXType.ORDER_REQUEST_RESULT)) {
            processOrderRequestResultPkt(pkt);
        } else if (pkt.msgType.equals(FXType.CANCEL_REJECT)) {
            processOrderCancelRejectPkt(pkt);
        } else if (pkt.msgType.equals(FXType.BUSINESS_MSG_REJECT)) {
            processBusinessMsgRejectPkt(pkt);
        } else {
            processUnknownPkt(pkt);
        }
    }

    public void processUnknownPkt(FXPacket pkt) throws Exception {
        System.out.println("processUnknownPkt: " + pkt.toString());
    }

    public void processBrokenPkt(FXPacket pkt) throws Exception {
        System.out.println("processBrokenPkt: " + pkt.toString());
    }

    public void processTestPkt(FXPacket pkt) throws Exception {
        System.out.println("processTestPkt: " + pkt.toString());
    }

    public void processHeartBeatTestPkt(FXPacket pkt) throws Exception {
        System.out.println("processHeartBeatTestPkt: " + pkt.toString());
        HashMap hm = new HashMap();
        hm.put(FXTag.TestReqID, "TEST");
        FXPacket heartBeatTestPkt = new FXPacket(FXType.HEARTBEAT_TEST, hm);
        heartBeatTestPkt.sendOut(jServiceBase.out);
        System.out.println("heartBeatTestpkt sent out: " + heartBeatTestPkt.toString());
    }

    public void processHeartBeatPkt(FXPacket pkt) throws Exception {
        System.out.println("processHeartBeatPkt: " + pkt.toString());
        this.jServiceBase.lastHeartBeateTS = System.currentTimeMillis();
    }

    public void processResendRequestPkt(FXPacket pkt) throws Exception {
        //TODO: record or resend
        System.out.println("processResendRequestPkt: " + pkt.toString());
        int newCounter = Integer.parseInt(pkt.body.get(FXTag.BeginSeqNo).toString());
        FXCounter.setCounter(newCounter);
    }

    public void processResetReqNumberPkt(FXPacket pkt) throws Exception {
        //TODO: record or resend
        System.out.println("processResetReqNumberPkt: " + pkt.toString());
        int newSeqNo = Integer.parseInt(pkt.body.get(FXTag.NewSeqNo).toString());
        FXCounter.setCounter(newSeqNo);
    }

    public void processLogoutPkt(FXPacket pkt) throws Exception {
        //TODO: save whole status
        jServiceBase.logout();
        System.out.println("processLogoutPkt: " + pkt.toString());
    }

    public void processAccountRequestResultPkt(FXPacket pkt) throws Exception {
        //TODO: record
        System.out.println("processAccountRequestResultPkt: " + pkt.toString());
        int posReqType = Integer.parseInt(pkt.body.get(FXTag.PosReqType).toString());
//        if (0 == posReqType) {//shares situation
//            if (pkt.body.containsKey(FXTag.Symbol)) {
//                Global.accountInfo.setShareStatus(pkt.body);
//            }
//        } else if (9 == posReqType) {//account status
//            Global.accountInfo.setAccountStatus(pkt.body);
//        } else {
//            System.out.println("processAccountRequestResultPkt error: unknown PosReqType " + posReqType);
//        }
    }

    public void processOrderRequestResultPkt(FXPacket pkt) throws Exception {
        //TODO: record and process under different condition
//        System.out.println("processOrderRequestResultPkt: " + pkt.toString());
//        int orderStatus = Integer.parseInt(pkt.body.get(FXTag.OrdStatus).toString());
//        String clOrdID = pkt.body.get(FXTag.ClOrdID).toString();
//        Global.orderInfo.delPendingOrder(clOrdID);
//        if (pkt.body.containsKey(FXTag.OrigClOrdID)) {
//            String origClOrdID = pkt.body.get(FXTag.OrigClOrdID).toString();
//            Global.orderInfo.delPendingOrder(origClOrdID);
//        }
//        if (0 == orderStatus) {
//            Global.orderInfo.setConfirmOrder(clOrdID, OrderStatus.PLACE_CONFIRM, pkt.body);
//            System.out.println("Order confirmed: " + clOrdID);
//        } else if (1 == orderStatus) {
//            Global.orderInfo.setConfirmOrder(clOrdID, OrderStatus.PARTIAL_FILLED, pkt.body);
//            System.out.println("Order partial filled: " + clOrdID);
//        } else if (2 == orderStatus) {
//            if (pkt.body.containsKey(FXTag.OrigClOrdID)) {
//                Global.orderInfo.setConfirmOrder(clOrdID, OrderStatus.CANCEL_REJECT, pkt.body);
//            } else {
//                Global.orderInfo.setConfirmOrder(clOrdID, OrderStatus.FULL_FILLED, pkt.body);
//            }
//            System.out.println("Order full filled: " + clOrdID);
//        } else if (4 == orderStatus) {
//            Global.orderInfo.setConfirmOrder(clOrdID, OrderStatus.CANCELED, pkt.body);
//            System.out.println("Order canceled: " + clOrdID);
//        } else if (6 == orderStatus) {
//            Global.orderInfo.setConfirmOrder(clOrdID, OrderStatus.WAIT_CANCEL_CONFIRM, pkt.body);
//            System.out.println("Order to be canceled: " + clOrdID);
//        } else if (8 == orderStatus) {
//            if (pkt.body.containsKey(FXTag.OrigClOrdID)) {
//                Global.orderInfo.setConfirmOrder(clOrdID, OrderStatus.CANCEL_REJECT, pkt.body);
//            } else {
//                Global.orderInfo.setConfirmOrder(clOrdID, OrderStatus.PLACE_REJECT, pkt.body);
//            }
//            System.out.println("Order rejected: " + clOrdID);
//        } else {
//            System.out.println("Order status unknown: " + clOrdID + ", status: " + orderStatus);
//        }
    }

    public void processOrderCancelRejectPkt(FXPacket pkt) throws Exception {
        //TODO: record and resend
        System.out.println("processOrderCancelRejectPkt: " + pkt.toString());
    }

    public void processSessionRejectPkt(FXPacket pkt) throws Exception {
        //TODO: record and resend
        System.out.println("processSessionRejectPkt: " + pkt.toString());
    }

    public void processBusinessMsgRejectPkt(FXPacket pkt) throws Exception {
        //TODO: record and resend
//        JPacket oriPkt = Global.sentPktCachePool.get(pkt);
        System.out.println("processBusinessMsqRejectPkt: " + pkt.toString());
    }

    @Override
    public void run() {
        while (!Thread.interrupted()) {
            try {
                FXPacket pkt = this.jServiceBase.getInPacketQueue().take();
                System.out.println(this.getName() + " received a pkt!");
                this.processPkt(pkt);
            } catch (Exception e) {
            }
        }
    }
}