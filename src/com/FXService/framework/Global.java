/*
 * Global.java
 *
 * Created on January 16, 2008, 2:05 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.FXService.framework;
import org.thavam.util.concurrent.BlockingHashMap;

import javax.net.SocketFactory;
import javax.net.ssl.SSLSocketFactory;


public class Global {    
    public static SSLSocketFactory socketFactory = (SSLSocketFactory)SSLSocketFactory.getDefault();
    
    public static final String productName = "FX";
    public static final String serviceName = "FXService";
    public static final String serviceId = "2";
    public static String fixServerAddr = "fix-ate.lmaxtrader.com";
    public static int fixServerPort = 5001;
    public static String pServiceAddr = "";
    public static String redisAddr = "";
    public static int redisPort = 6379;
    public static int redisDB = 1;
    public static int webapiPort = 9100;
    public static String[] mongoDBServers = {};
    public static int mongoDBPort = 27017;
    public static String mongoDBName = "mydb";
    //FIX
    public static String fixProtocalVersion = "FIX.4.4";
    public static String user = "testFX";
    public static String pwd = "Xj$nbyndfwlb#C";
    public static String senderCompID = "fq0619";//"shiroh880527";
    public static String targetCompID = "LMXBD";
    public static String heartBeatInterval = "5";
    //JServiceBase
    public static int inPacketQueueSize = 128;
    public static int processorThreadsSize = 5;
    public static int sentPktCachePoolSize = 128;
}
