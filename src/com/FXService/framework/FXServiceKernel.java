/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.framework;

import com.FXService.util.FXOrder;
import com.FXService.util.FXUtil;
import java.util.Calendar;
import java.util.TimeZone;
import org.apache.log4j.Logger;

/**
 * @author qiangfu
 * @version 0.1
 * @since Mar 1, 2014
 */
public abstract class FXServiceKernel implements Runnable {

    static final Logger logger = Logger.getLogger(FXServiceKernel.class);
    public boolean isRun = false;
    protected Thread runThread = null;

    public FXServiceKernel() {
    }

    public void startSvr() {
        forceStop();
        runThread = new Thread(this, "JServiceKernel runThread");
        isRun = true;
        runThread.start();
        startThreads();
    }

    public synchronized String generateID() {
        String ts = "" + FXUtil.getCurrentTimestampLong();
//        String tail = FXRedis.getIDTail();
        String id = "" + ts;
        return id;
    }

    public static boolean underTimeLimitation() {
        Calendar now = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        int hour = now.get(Calendar.HOUR_OF_DAY);
        int minute = now.get(Calendar.MINUTE);
        int day = now.get(Calendar.DAY_OF_WEEK);
        if (hour >= 2 && hour < 6 && day >= 2 && day <= 6) {
            return true;
        } else if (hour == 1 && minute >= 30 && day >= 2 && day <= 6) {
            return true;
        } else if (hour == 6 && minute < 56 && day >= 2 && day <= 6) {
            return true;
        }
        return false;
    }

    public static double calcCommissionFeeStock(int amount, double price, int side) {
        double fee = amount * price * 0.0006;
        if (fee < 5) {
            fee = 5;
        }
//        fee += FXUtil.roundCeil(amount, 1000, "0") * 0.0006;
//        if (side == FXOrder.SIDE_SELL) {
//            fee += amount * price * 0.001;
//        }
        return FXUtil.roundCeil(fee, 0.01, "0.00");
    }

    public static double calcCommissionFeeBond(int amount, double price, int side, String eId) {
        double fee = amount * price * 0.0002;
        if (eId.toLowerCase().equals("sh")) {
            fee = (fee < 1) ? 1 : fee;
        } else if (eId.toLowerCase().equals("sz")) {
            fee = (fee < 0.1) ? 0.1 : fee;
        }
        return FXUtil.roundCeil(fee, 0.01, "0.00");
    }

    public static double calcCommissionFeeCBond(int amount, double price, int side, String eId) {
        double fee = 0;
        if (eId.toLowerCase().equals("sh")) {
            fee = amount * price * 0.0002;
            fee = (fee < 1) ? 1 : fee;
        } else if (eId.toLowerCase().equals("sz")) {
            fee = amount * price * 0.001;
            fee = (fee < 0.1) ? 0.1 : fee;
        }
        return FXUtil.roundCeil(fee, 0.01, "0.00");
    }

    public void forceStop() {
        try {
            if (runThread != null) {
                if (runThread.isAlive()) {
                    runThread.stop();
                }
            }
            runThread = null;
        } catch (Exception e) {
            ;
        }
    }

    public void stopSvr() {
        isRun = false;
    }

    public void run() {
        try {
            System.out.println("[Main] starting!");
            while (isRun) {
                System.out.println("[Main] running, update every 30s!");
                logger.info("[Main] running, update every 30s!");
                Thread.sleep(1000 * 30);
            }
        } catch (Exception e) {
            System.out.println("[Main] Main thread is about over, exception occured!");
            logger.error("[Main] Main thread is about over, exception occured!");
        }

        System.out.println("[Main] Main thread is over!");
        logger.info("[Main] Main thread is over!");
    }

    protected void startThreads() {
    }

    protected void stopThreads() {
    }
}
