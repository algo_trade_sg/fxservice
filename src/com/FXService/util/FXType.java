/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.util;

/**
 * @author qiangfu
 * @version 0.1
 * @since Apr 1, 2014
 */

public class FXType {
    public final static String DEBUG = "DEBUG";
    public final static String ACCOUNT_REQUEST = "UAN";
    public final static String ACCOUNT_REQUEST_RESULT = "UAP";
    public final static String LOGIN = "A";
    public final static String PLACE_SINGLE_ORDER = "D";
    public final static String CANCEL_SINGLE_ORDER = "F";
    public final static String CHECK_ORDER_STATUS = "H";
    public final static String BUSINESS_MSG_REJECT = "j";
    public final static String HEARTBEAT = "0";
    public final static String HEARTBEAT_TEST = "1";
    public final static String RESEND_REQUEST = "2";
    public final static String SESSION_REJECT = "3";
    public final static String RESET_SEQ_NUMBER = "4";
    public final static String LOGOUT = "5";
    public final static String ORDER_REQUEST_RESULT = "8";
    public final static String CANCEL_REJECT = "9";
}
