///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package com.FXService.util;
//
//import java.math.BigDecimal;
//
///**
// *
// * @author qiangfu
// */
//public class FXPair {
//
//    public String from;
//    public String to;
//    public int valuePrecision;
//    public int allowDirection;
//    private BigDecimal price = new BigDecimal("0");
//    private int amount = 0;
//    private FXOrder fXOrder = null;
//
//    public FXPair(String from, String to, int direction, int precision) {
//        this.from = from;
//        this.to = to;
//        this.valuePrecision = precision;
//        this.allowDirection = direction;
//        if (1 == direction) {
//            this.fXOrder = new FXOrder(to, from, "BUY");
//        } else {
//            this.fXOrder = new FXOrder(from, to, "SELL");
//        }
//    }
//
//    public void setPrice(double price) {
//        this.fXOrder.m_lmtPrice = price;
//        this.price = new BigDecimal("" + price);
//    }
//
//    public BigDecimal getPrice() {
//        if (0 == this.allowDirection) {
//            return this.price;
//        } else {
//            if (this.price.doubleValue() <= 0) {
//                return new BigDecimal("0");
//            }
//            return (new BigDecimal("1")).divide(this.price, 2 * this.valuePrecision, BigDecimal.ROUND_HALF_DOWN);
//        }
//    }
//
//    public void setAmount(int amount) {
//        this.amount = amount;
//    }
//
//    public int getAmount() {
//        if (0 == this.allowDirection) {
//            return (int) (this.amount / this.getPrice().doubleValue());
//        } else {
//            return this.amount;
//        }
//    }
//
//    public FXOrder getOrder() {
//        return this.fXOrder;
//    }
//
//    @Override
//    public String toString() {
//        return "{from=" + this.from + ",to=" + this.to + ",valuePrecision=" + this.valuePrecision + ",allowDirection=" + this.allowDirection + ",price=" + this.getPrice() + ",amount=" + this.getAmount() + "," + this.fXOrder.toString() + "}";
//    }
//
//    public static void main(String[] args) throws Exception {
//    }
//}
