/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.util;

import com.FXService.framework.FXServiceGroup;
import com.FXService.framework.Global;
import java.io.DataOutputStream;
import java.util.HashMap;
import org.thavam.util.concurrent.BlockingHashMap;

/**
 * @author qiangfu
 * @version 0.1
 * @since DEC 19, 2013
 * 
 * @notice NOT WORK ON LMAX
 */

public class FXAccount {
    
    public HashMap accountStatus = null;
    public HashMap shareStatus = null;
    DataOutputStream out = null;

    public FXAccount(DataOutputStream out) {
        this.out = out;
    }

    public static String getNewReqID() {//TODO: to be implemented by redis
        long timestamp = System.currentTimeMillis();
        return Long.toString(timestamp);
    }

    public void requestAccountStatus() {
        try {
            HashMap hm = new HashMap();
            hm.put(FXTag.PosReqID, getNewReqID());
            hm.put(FXTag.PosReqType, "9");
            FXPacket pkt = new FXPacket(FXType.ACCOUNT_REQUEST, hm);
            pkt.sendOut(out);
            System.out.println("requestAccountStatusPkt sent out: " + pkt.toString());
        } catch (Exception e) {
            System.out.println("requestAccountStatusPkt error: " + e.getMessage());
        }
    }

    public void requestShareStatus() {
        try {
            HashMap hm = new HashMap();
            hm.put(FXTag.PosReqID, getNewReqID());
            hm.put(FXTag.PosReqType, "0");
            FXPacket pkt = new FXPacket(FXType.ACCOUNT_REQUEST, hm);
            pkt.sendOut(out);
            System.out.println("requestShareStatusPkt sent out: " + pkt.toString());
        } catch (Exception e) {
            System.out.println("requestShareStatusPkt error: " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        try {
        } catch (Exception e) {
        }
    }
}
