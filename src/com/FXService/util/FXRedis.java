//package com.FXService.util;
//
//import com.FXService.framework.FXKernelWrapper;
//import com.FXService.framework.Global;
//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.*;
//import redis.clients.jedis.*;
//
////import org.apache.commons.configuration.CompositeConfiguration;
//import org.apache.commons.configuration.Configuration;
//import org.apache.commons.configuration.PropertiesConfiguration;
//
///**
// * This class connect and manipulate the Redis database, mainly for those
// * login/logout records
// *
// * @author chuck
// * @version 0.2
// * @since Jan 3, 2012
// */
//public class FXRedis {
//    
//    private static String redisServer = "127.0.0.1";
//    private static int redisPort = 6379;
//    private static int redisDatabaseUsing = 1;
//    private static String frequencyCountHash = "frequency:count:hash";
//    private static String transactionCountHash = "transaction:count:hash";
//    private static String accountBalanceHashPrefix = "account:balance:";
//    private static String accountBalanceHash = "account:balance:currency";
//    private static String unsettlementCountHashPrefix = "unsettlement:count:";
//    private static String stoplossCountSortedSetPrefix = "stoploss:count:";
//    private static String stoplossOrderHashPrefix = "stoploss:order:";
//    private static String stoplossCountHashPrefix = "stoploss:daily:count";
//    private static String serializedOrdersHashPrefix = "serialized:orders";
//    private static final FXRedis INSTANCE = new FXRedis();
//    private JedisPool pool = null;
//
//    static {
//        try {
//            redisServer = Global.redisServer;
//            redisPort = Global.redisPort;
//            redisDatabaseUsing = 1;
//        } catch (Exception e) {
//            System.out.println("Redis driver init error!");
//        }
//    }
//
//    private FXRedis() {
//        JedisPoolConfig config = new JedisPoolConfig();
//        config.setTestOnBorrow(true);
//        pool = new JedisPool(config, redisServer, redisPort);
//    }
//
//    public static JedisPool getJedisPool() {
//        return INSTANCE.pool;
//    }
//
//    public static void setSerializedOrders(int clientId, int orderId, QuoteRequest qr) {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
////        int size = qr.getSize();
//        int size = qr.getRemain();
//        double rate = qr.getRate();
//        String action = qr.getAction();
//        String symbol = qr.getSymbol();
//        String currency = qr.getCurrency();
//        // write date to redis
//        try {
//            String hkey = clientId + ":" + orderId;
//            String sOrder = size + ":" + rate + ":" + action + ":" + symbol + ":" + currency;
//            jedis.select(redisDatabaseUsing);
//            jedis.hset(serializedOrdersHashPrefix, hkey, sOrder);
//            System.out.println("[RedisDriver] setSerializedOrders " + clientId + " " + orderId);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            System.out.println("[RedisDriver] [Error] setSerializedOrders Exception: " + e.getMessage());
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    public static void updateSerializedOrders(int clientId, int oldOrderId, QuoteRequest qr) {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        int orderId = qr.getOrderId();
////        int size = qr.getSize();
//        int size = qr.getRemain();
//        double rate = qr.getRate();
//        String action = qr.getAction();
//        String symbol = qr.getSymbol();
//        String currency = qr.getCurrency();
//        // write date to redis
//        try {
//            String newHkey = clientId + ":" + orderId;
//            String oldHkey = clientId + ":" + oldOrderId;
//            String sOrder = size + ":" + rate + ":" + action + ":" + symbol + ":" + currency;
//            jedis.select(redisDatabaseUsing);
//            long ret1 = jedis.hdel(serializedOrdersHashPrefix, oldHkey);
//            long ret2 = jedis.hset(serializedOrdersHashPrefix, newHkey, sOrder);
////            Pipeline ppl = jedis.pipelined();
////            ppl.hdel(serializedOrdersHashPrefix ,oldHkey);
////            ppl.hset(serializedOrdersHashPrefix ,newHkey, sOrder);
////            ppl.exec();
//
//            System.out.println("[RedisDriver] updateSerializedOrders " + clientId + " " + oldOrderId + " " + orderId + " " + ret1 + " " + ret2);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            System.out.println("[RedisDriver] [Error] updateSerializedOrders Exception: " + e.getMessage());
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    public static Object[] getAllSerializedOrders() {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//        Object[] oOrders = null;
//        // read date to redis
//        try {
//            jedis.select(redisDatabaseUsing);
//            oOrders = jedis.hkeys(serializedOrdersHashPrefix).toArray();
//            System.out.println("[RedisDriver] getAllSerializedOrders");
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            System.out.println("[RedisDriver] [Error] getAllSerializedOrders Exception: " + e.getMessage());
//        } finally {
//            pool.returnResource(jedis);
//        }
//        return oOrders;
//    }
//
//    public static QuoteRequest getSerializedOrders(int clientId, int orderId) {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//        QuoteRequest qr = null;
//        // read date to redis
//        try {
//            String hkey = clientId + ":" + orderId;
//            jedis.select(redisDatabaseUsing);
//            String sOrder = jedis.hget(serializedOrdersHashPrefix, hkey);
//            String[] parts = sOrder.split(":");
//
//            int size = Integer.parseInt(parts[0]);
//            double rate = Double.parseDouble(parts[1]);
//            String action = parts[2];
//            String symbol = parts[3];
//            String currency = parts[4];
//
//            System.out.println("[RedisDriver] getSerializedOrders " + clientId + " " + orderId);
//            qr = new QuoteRequest(clientId, orderId, size);
//            qr.setFXContract(symbol, currency, rate, action);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            System.out.println("[RedisDriver] [Error] getSerializedOrders Exception: " + e.getMessage());
//        } finally {
//            pool.returnResource(jedis);
//        }
//        return qr;
//    }
//
//    public static void delSerializedOrders(int clientId, int orderId) {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        try {
//            String hkey = clientId + ":" + orderId;
//            jedis.select(redisDatabaseUsing);
//            jedis.hdel(serializedOrdersHashPrefix, hkey);
//
//            System.out.println("[RedisDriver] delSerializedOrders " + clientId + " " + orderId);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            System.out.println("[RedisDriver] [Error] delSerializedOrders Exception: " + e.getMessage());
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    public static void delSerializedOrdersWithValueChecking(int clientId, int orderId,
//            int size, double rate, String action, String symbol, String currency) {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        try {
////            String sOrder = size+":"+rate+":"+action+":"+symbol+":"+currency;
//            String hkey = clientId + ":" + orderId;
//            jedis.select(redisDatabaseUsing);
//            String sOrder = jedis.hget(serializedOrdersHashPrefix, hkey);
//            String[] parts = sOrder.split(":");
//
//            int oSize = Integer.parseInt(parts[0]);
//            double oRate = Double.parseDouble(parts[1]);
//            String oAction = parts[2];
//            String oSymbol = parts[3];
//            String oCurrency = parts[4];
//
//            long ret = 0;
//            if (symbol.equals(oSymbol) && currency.equals(oCurrency) && action.equals(oAction) && oSize == size) {
//                ret = jedis.hdel(serializedOrdersHashPrefix, hkey);
//            }
//
//            System.out.println("[RedisDriver] delSerializedOrdersWithValueChecking " + clientId + " " + orderId + " " + ret);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            System.out.println("[RedisDriver] [Error] delSerializedOrdersWithValueChecking Exception: " + e.getMessage());
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    public static void setDailyAccountBalance(String currency, String balance) {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        String dateStr = "";
//        Date date = new Date();
//        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
//        // write date to redis
//        try {
//
//            dateStr = sdf.format(date);
//            jedis.select(redisDatabaseUsing);
//            jedis.hset(accountBalanceHashPrefix + currency, dateStr, balance);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    public static void setAccountBalance(String currency, String balance) {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        // write date to redis
//        try {
//            jedis.select(redisDatabaseUsing);
//            jedis.hset(accountBalanceHash, currency, balance);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    public static void setFrequencyCount() {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        String dateStr = "";
//        Date date = new Date();
//        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH");
//        // write date to redis
//        try {
//
//            dateStr = sdf.format(date);
//            jedis.select(redisDatabaseUsing);
//            jedis.hincrBy(frequencyCountHash, dateStr, 1);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    public static void incrStopLossCount() {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        String dateStr = "";
//        long currentTime = System.currentTimeMillis();
//        currentTime -= 1000 * 60 * 60 * 4;
//        Date date = new Date(currentTime);
//        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//        // write date to redis
//        try {
//
//            dateStr = sdf.format(date);
//            jedis.select(redisDatabaseUsing);
//            jedis.hincrBy(stoplossCountHashPrefix, dateStr, 1);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    public static void setStoplossCount(int orderId, int lossLevel) {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        String dateStr = "";
//        Date date = new Date();
//        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//        // write date to redis
//        try {
//
//            dateStr = sdf.format(date);
//            jedis.select(redisDatabaseUsing);
//            String key1 = stoplossCountSortedSetPrefix + dateStr;
//            String key2 = stoplossOrderHashPrefix + dateStr;
//            jedis.zadd(key1, (double) lossLevel, "" + orderId);
//            String curStoplossOrder = jedis.hget(key2, "" + orderId);
//            if (curStoplossOrder == null) {
//                curStoplossOrder = "" + lossLevel;
//                jedis.hset(key2, "" + orderId, curStoplossOrder);
//            } else {
//                String[] split = curStoplossOrder.split(",");
//                if (Integer.valueOf(split[split.length - 1]) != lossLevel) {
//                    curStoplossOrder += "," + lossLevel;
//                    jedis.hset(key2, "" + orderId, curStoplossOrder);
//                }
//            }
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    //IMPORTANT: based on GROSS SETTLEMENT
//    public static void setUnsettlementCount(String symbol, String currency, boolean isBuy) {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        String dateStr = "";
//        Date date = new Date();
//        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//        // write date to redis
//        try {
//
//            dateStr = sdf.format(date);
//            jedis.select(redisDatabaseUsing);
//            jedis.hincrBy(unsettlementCountHashPrefix + dateStr, symbol + ":" + currency, isBuy ? 1 : -1);
////            String hk1 = "d:"+from;
////            String hk2 = "i:"+to;
////            Pipeline ppl = jedis.pipelined();
////            ppl.hincrBy(unsettlementCountHashPrefix+dateStr, hk1, -1);
////            ppl.hincrBy(unsettlementCountHashPrefix+dateStr, hk2, 1);
////            ppl.exec();
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    public static int getAllUnsettlementCount() {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        String dateStr = "";
//        int count = 0;
//        Date date = new Date();
//        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//        // write date to redis
//        try {
//
//            dateStr = sdf.format(date);
//            jedis.select(redisDatabaseUsing);
//            List<String> vals = new ArrayList<String>();
//            vals = jedis.hvals(unsettlementCountHashPrefix + dateStr);
//            for (int i = 0; i < vals.size(); i++) {
//                count += Math.abs(Integer.valueOf((String) vals.get(i)));
//            }
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//        } finally {
//            pool.returnResource(jedis);
//        }
//        return count;
//    }
//
//    public static void setTransactionCount() {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        String dateStr = "";
//        long currentTime = System.currentTimeMillis();
//        currentTime -= 1000 * 60 * 60 * 4;
//        Date date = new Date(currentTime);
//        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//        // write date to redis
//        try {
//
//            dateStr = sdf.format(date);
//            jedis.select(redisDatabaseUsing);
//            jedis.hincrBy(transactionCountHash, dateStr, 1);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    public static int getTransactionCount() {
//        // set up the JedisPool and Jedis
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//
//        int transactionCount = 0;
//        String dateStr = "";
//        long currentTime = System.currentTimeMillis();
//        currentTime -= 1000 * 60 * 60 * 4;
//        Date date = new Date(currentTime);
//        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//        // read date to redis
//        try {
//            dateStr = sdf.format(date);
//            jedis.select(redisDatabaseUsing);
//            transactionCount = Integer.parseInt(jedis.hget(transactionCountHash, dateStr));
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//        } finally {
//            pool.returnResource(jedis);
//        }
//        return transactionCount;
//    }
//
//    //newly added, acturally using
//    public static void saveHashTableFromHashMap(String hkey, HashMap<String, String> hm) {
//        if (hm.isEmpty()) {
//            return;
//        }
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//        try {
//            jedis.select(redisDatabaseUsing);
//            Iterator it = hm.entrySet().iterator();
//            Pipeline ppl = jedis.pipelined();
//            while (it.hasNext()) {
//                Map.Entry entry = (Map.Entry) it.next();
//                String key = entry.getKey().toString();
//                String val = entry.getValue().toString();
//                ppl.hset(hkey, key, val);
//            }
//            ppl.sync();
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            e.printStackTrace();
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//
//    public static HashMap<String, String> loadHashMapFromHashTable(String hkey) {
//        if (hkey.isEmpty()) {
//            return null;
//        }
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//        HashMap<String, String> hm = new HashMap<String, String>();
//        try {
//            jedis.select(redisDatabaseUsing);
//            hm = (HashMap<String, String>) jedis.hgetAll(hkey);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            e.printStackTrace();
//        } finally {
//            pool.returnResource(jedis);
//        }
//        return hm;
//    }
//
//    public static void removeFromHashTable(String hkey, String key) {
//        if (hkey.isEmpty() || key.isEmpty()) {
//            return;
//        }
//        JedisPool pool = FXRedis.getJedisPool();
//        Jedis jedis = pool.getResource();
//        try {
//            jedis.select(redisDatabaseUsing);
//            jedis.hdel(hkey, key);
//        } catch (Exception e) {
//            pool.returnBrokenResource(jedis);
//            e.printStackTrace();
//        } finally {
//            pool.returnResource(jedis);
//        }
//    }
//}
