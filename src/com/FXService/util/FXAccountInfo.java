/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.util;

import java.util.HashMap;

/**
 *
 * @author qiangfu
 */
public class FXAccountInfo {

    public double fundValue = 0;//amount of money
    public double fundLeftValue = 0;//money left
    public double fundAvaliableValue = 0;//money left that can be use
    public double fundFrozenValue = 0;//money frozen by purchase action
    public double totalValue = 0;//total value incl. money and stock
    public double stockValue = 0;//total stock market value
    public String ts = "1970/01/01 00:00:00";

    public FXAccountInfo(HashMap hm) {
        if (hm.containsKey("707_F")) {
            this.fundValue = Double.parseDouble(hm.get("707_F").toString());
        }
        if (hm.containsKey("707_FB")) {
            this.fundLeftValue = Double.parseDouble(hm.get("707_FB").toString());
        }
        if (hm.containsKey("707_FAV")) {
            this.fundAvaliableValue = Double.parseDouble(hm.get("707_FAV").toString());
        }
        if (hm.containsKey("707_FBF")) {
            this.fundFrozenValue = Double.parseDouble(hm.get("707_FBF").toString());
        }
        if (hm.containsKey("707_SV")) {
            this.stockValue = Double.parseDouble(hm.get("707_SV").toString());
        }
        if (hm.containsKey("707_MV")) {
            this.totalValue = Double.parseDouble(hm.get("707_MV").toString());
        }
        if (hm.containsKey("ts")) {
            this.ts = hm.get("ts").toString();
        }
    }

    public void set(HashMap hm) {
        if (hm.containsKey("707_F")) {
            this.fundValue = Double.parseDouble(hm.get("707_F").toString());
        }
        if (hm.containsKey("707_FB")) {
            this.fundLeftValue = Double.parseDouble(hm.get("707_FB").toString());
        }
        if (hm.containsKey("707_FAV")) {
            this.fundAvaliableValue = Double.parseDouble(hm.get("707_FAV").toString());
        }
        if (hm.containsKey("707_FBF")) {
            this.fundFrozenValue = Double.parseDouble(hm.get("707_FBF").toString());
        }
        if (hm.containsKey("707_SV")) {
            this.stockValue = Double.parseDouble(hm.get("707_SV").toString());
        }
        if (hm.containsKey("707_MV")) {
            this.totalValue = Double.parseDouble(hm.get("707_MV").toString());
        }
    }

    @Override
    public String toString() {
        String ret = fundValue + " " + fundLeftValue + " " + fundAvaliableValue + " " + fundFrozenValue + " " + stockValue + " " + totalValue + " " + ts;
        return ret;
    }
}
