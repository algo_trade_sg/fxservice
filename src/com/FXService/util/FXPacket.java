/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.util;

import java.io.*;
import java.util.*;
import org.apache.commons.lang.ArrayUtils;
import com.FXService.framework.Global;

/**
 * @author qiangfu
 * @version 0.1
 * @since Apr 1, 2014
 */

public class FXPacket {
    //required blocks
    public String beginString = Global.fixProtocalVersion;
    public int bodyLength = 0;
    public String msgType = FXType.DEBUG;
    public String senderCompID = Global.senderCompID;
    public String targetCompID = Global.targetCompID;
    public int msgSeqNum = 0;//JCounter.getCounter();//set when sendout?
    public String sendingTime = FXUtil.getCurrentTimestamp();
    public Boolean isBroken = false;
    public HashMap body = new HashMap();
    
    public static byte SEPRATOR = 0x01;
    public static byte SEPRATOR_PRINTABLE = '|';
    public static byte LINKER = '=';
        
    public FXPacket(String msgType, HashMap hm) {
        this.msgType = msgType;
        body = hm;
        msgSeqNum = FXCounter.getCounter();
        bodyLength = calcBodyLength();
    }
    
    public FXPacket(String msgType, byte[] bytes) {
        this.msgType = msgType;
        body = bytesToHashMap(bytes);
        msgSeqNum = FXCounter.getCounter();
        bodyLength = calcBodyLength();
    }
    
    public FXPacket(DataInputStream in) throws Exception {
        byte lookAhead;
        String pairStr = "";
        String tmpStr = "";
        //get beginString
        do{
            //in.read();
            lookAhead = in.readByte();
            pairStr += (char)lookAhead;
        }while(lookAhead != SEPRATOR);
        beginString = pairStr.substring(pairStr.indexOf(LINKER)+1, pairStr.indexOf(SEPRATOR));    
        tmpStr += pairStr;//+(char)sepratorPrintable;
        //get bodyLength
        pairStr = "";
        do{
            lookAhead = (byte)in.readByte();
            pairStr += (char)lookAhead;
        }while(lookAhead != SEPRATOR);
        bodyLength = Integer.parseInt(pairStr.substring(pairStr.indexOf(LINKER)+1, pairStr.indexOf(SEPRATOR)));        beginString = pairStr.substring(pairStr.indexOf(LINKER)+1, pairStr.indexOf(SEPRATOR));    
        tmpStr += pairStr;//+(char)sepratorPrintable;
        //get payload
        byte[] payload = new byte[bodyLength];
        in.readFully(payload, 0, bodyLength);
        //get header
        body = bytesToHashMapWithGroup(payload);
        msgSeqNum = Integer.valueOf(body.get(FXTag.MsgSeqNum).toString());
        body.remove(FXTag.MsgSeqNum);
        msgType = body.get(FXTag.MsgType).toString();
        body.remove(FXTag.MsgType);
        sendingTime = body.get(FXTag.SendingTime).toString();
        body.remove(FXTag.SendingTime);
        senderCompID = body.get(FXTag.SenderCompID).toString();
        body.remove(FXTag.SenderCompID);
        targetCompID = body.get(FXTag.TargetCompID).toString();
        body.remove(FXTag.TargetCompID);
        //get checkSum
        pairStr = "";
        do{
            lookAhead = in.readByte();
            pairStr += (char)lookAhead;
        }while(lookAhead != SEPRATOR);
        int checkSum = Integer.parseInt(pairStr.substring(pairStr.indexOf(LINKER)+1, pairStr.indexOf(SEPRATOR)));
        //calculate checkSum
        byte[] tmpBytes = new byte[tmpStr.length()];
        for(int i = 0; i < tmpStr.length(); i++){
            if(tmpStr.charAt(i) == (char)SEPRATOR_PRINTABLE)
                tmpBytes[i] = SEPRATOR;
            else tmpBytes[i] = (byte)tmpStr.charAt(i);
        }
        byte[] bytes = (byte[]) ArrayUtils.addAll(tmpBytes, payload);
        int calcCheckSum = FXUtil.calcCheckSum(bytes);
        //set broken JPacket
        if(calcCheckSum != checkSum)
            isBroken = true;
    }

    public void sendOut(DataOutputStream out) throws Exception {
        byte[] bytes = pktToBytes();
        out.write(bytes);
        out.flush();
//        Global.sentPktCachePool.put(this.msgSeqNum, this);
    }

    @Override
    public String toString() {
        String ret = null;
        try{
            byte[] bytes = pktToBytes();
            for(int i = 0; i < bytes.length; i++){
                if((char)bytes[i] == SEPRATOR)
                    bytes[i] = SEPRATOR_PRINTABLE;
            }
            ret = new String(bytes, "GBK");
        }catch(Exception e){
            
        }
        return "pkt = {"+ret+"}";
    }
        
    public static HashMap bytesToHashMap(byte[] bytes) {
        String tmpStr = "";
        String keyStr = "";
        String valStr = "";
        HashMap hashmap = new HashMap();
        Boolean waitLinker = true;
        for(int i = 0;i < bytes.length; i++){
            if(bytes[i] == LINKER && waitLinker){
                waitLinker = false;
                keyStr = tmpStr;
                tmpStr = "";
            }else if(bytes[i] == SEPRATOR){
                waitLinker = true;
                valStr = tmpStr;
                tmpStr = "";
                hashmap.put(keyStr, valStr);
            }else{
                tmpStr += (char)bytes[i];
            }
        }
        return hashmap;
    }
    
    //this function should be useful under only one condition: while reading from input stream
    public static HashMap bytesToHashMapWithGroup(byte[] bytes) {
        String tmpStr = "";
        String keyStr = "";
        String groupKeyStr = "";
        String valStr = "";
        String groupValStr = "";
        HashMap hashmap = new HashMap();
        Boolean waitLinker = true;
        for(int i = 0;i < bytes.length; i++){
            if(bytes[i] == LINKER && waitLinker){
                waitLinker = false;
                keyStr = tmpStr;
                tmpStr = "";
            }else if(bytes[i] == SEPRATOR){
                waitLinker = true;
                valStr = tmpStr;
                tmpStr = "";
                if(FXTag.GroupKeys.contains(keyStr)){
                    groupKeyStr = keyStr+"_"+valStr;
                    hashmap.put(groupKeyStr, "");//put empty string to take place
                }else if(FXTag.GroupVals.contains(keyStr)){
                    if(!groupKeyStr.equals("")){
                        hashmap.put(groupKeyStr, valStr);
                        groupKeyStr = "";
                    }else
                        groupValStr = valStr;
                }else
                    hashmap.put(keyStr, valStr);
            }else{
                tmpStr += (char)bytes[i];
            }
        }
        return hashmap;
    }
    
    public static byte[] hashMapToBytes(HashMap hm) {
        String tmpStr = "";
        String keyStr = "";
        String valStr = "";
        Iterator it = hm.entrySet().iterator(); 
        while(it.hasNext()){ 
            Map.Entry entry = (Map.Entry)it.next(); 
            keyStr = (String)entry.getKey(); 
            valStr = (String)entry.getValue(); 
            tmpStr = tmpStr+(keyStr+(char)LINKER+valStr+(char)SEPRATOR_PRINTABLE);
        } 
        byte[] bytes = new byte[tmpStr.length()];
        for(int i = 0; i < tmpStr.length(); i++){
            if(tmpStr.charAt(i) == (char)SEPRATOR_PRINTABLE)
                bytes[i] = SEPRATOR;
            else bytes[i] = (byte)tmpStr.charAt(i);
        }
        return bytes;
    }
    
//    public static byte[] hashMapToBytesWithGroup(HashMap hm) {
//        String tmpStr = "";
//        String keyStr = "";
//        String valStr = "";
//        Iterator it = hm.entrySet().iterator(); 
//        while(it.hasNext()){ 
//            Map.Entry entry = (Map.Entry)it.next(); 
//            keyStr = (String)entry.getKey(); 
//            valStr = (String)entry.getValue(); 
//            tmpStr = tmpStr+(keyStr+(char)linker+valStr+(char)sepratorPrintable);
//        } 
//        byte[] bytes = new byte[tmpStr.length()];
//        for(int i = 0; i < tmpStr.length(); i++){
//            if(tmpStr.charAt(i) == (char)sepratorPrintable)
//                bytes[i] = seprator;
//            else bytes[i] = (byte)tmpStr.charAt(i);
//        }
//        return bytes;
//    }
    
    private byte[] getHeadBytes() {
        String tmpStr = FXTag.BeginString+(char)LINKER+beginString+(char)SEPRATOR_PRINTABLE
                +FXTag.BodyLength+(char)LINKER+bodyLength+(char)SEPRATOR_PRINTABLE
                +FXTag.MsgType+(char)LINKER+msgType+(char)SEPRATOR_PRINTABLE
                +FXTag.SenderCompID+(char)LINKER+senderCompID+(char)SEPRATOR_PRINTABLE
                +FXTag.TargetCompID+(char)LINKER+targetCompID+(char)SEPRATOR_PRINTABLE
                +FXTag.MsgSeqNum+(char)LINKER+msgSeqNum+(char)SEPRATOR_PRINTABLE
                +FXTag.SendingTime+(char)LINKER+sendingTime+(char)SEPRATOR_PRINTABLE;
        byte[] bytes = new byte[tmpStr.length()];
        for(int i = 0; i < tmpStr.length(); i++){
            if(tmpStr.charAt(i) == (char)SEPRATOR_PRINTABLE)
                bytes[i] = SEPRATOR;
            else bytes[i] = (byte)tmpStr.charAt(i);
        }
        return bytes;
    } 
        
    public int calcBodyLength() {
        byte[] bytes = hashMapToBytes(body);
        HashMap tmpHm = bytesToHashMap(getHeadBytes());
        tmpHm.remove(FXTag.BeginString);
        tmpHm.remove(FXTag.BodyLength);
        byte[] tmpBytes = hashMapToBytes(tmpHm);
        return bytes.length + tmpBytes.length;
    }
        
    //add in CheckSum tag
    public byte[] pktToBytes() {
        byte[] head = getHeadBytes();
        byte[] bytes = (byte[]) ArrayUtils.addAll(head, hashMapToBytes(body));
        int checkSum = FXUtil.calcCheckSum(bytes);
        String tmpStr = FXTag.CheckSum+(char)LINKER+String.format("%03d", checkSum)+(char)SEPRATOR_PRINTABLE;
        byte[] tail = new byte[tmpStr.length()];
        for(int i = 0; i < tmpStr.length(); i++){
            if(tmpStr.charAt(i) == (char)SEPRATOR_PRINTABLE)
                tail[i] = SEPRATOR;
            else tail[i] = (byte)tmpStr.charAt(i);
        }
        return (byte[]) ArrayUtils.addAll(bytes, tail);
    }
    
    public static void main(String[] args) throws IOException{
//        System.out.println(bytesToHashMapWithGroup(bs));
        HashMap hm = new HashMap();
        hm.put(FXTag.EncryptMethod, "2");
        hm.put(FXTag.HeartBtInt, Global.heartBeatInterval);
        hm.put(FXTag.RawData, "Z:");
        hm.put(FXTag.ResetSeqNumFlag, "Y");
        System.out.println(""+hm.get("1"));
        FXPacket pkt = new FXPacket(FXType.LOGIN, hm);
        int em = Integer.parseInt(pkt.body.get(FXTag.EncryptMethod).toString());
        System.out.println(""+em);
    }
}
