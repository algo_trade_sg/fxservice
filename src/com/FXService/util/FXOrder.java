/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.util;

import com.FXService.common.BaseOrder;
import java.io.*;
import java.util.HashMap;
import org.apache.commons.codec.binary.Base64;

public class FXOrder extends BaseOrder {

    public static String ORDER_TYPE = "2";//limit order
    public static String HANDLINST = "1";//private directly

    public FXOrder() {
        super();
    }

    public FXOrder(String symbol, String currency, int side, int totalQuantity, double lmtPrice) {
        super();
        this.symbol = symbol;
        this.currency = currency;
        this.side = side;
        this.totalQuantity = totalQuantity;
        this.lmtPrice = lmtPrice;
        status = OrderStatus.INVALID;
    }

    public static String getNewClOrdID() {//TODO: to be implemented by redis
        long timestamp = System.currentTimeMillis();
        return Long.toString(timestamp);
    }

    public void placeOrder(DataOutputStream out) {
        orderId = getNewClOrdID();
        ct = FXUtil.getCurrentTimestamp();
        try {
            HashMap hm = new HashMap();
            hm.put(FXTag.ClOrdID, orderId);
            hm.put(FXTag.HandlInst, HANDLINST);
            hm.put(FXTag.OrdType, ORDER_TYPE);
            hm.put(FXTag.Side, Integer.toString(side));
            hm.put(FXTag.Symbol, symbol);
            hm.put(FXTag.TransactTime, FXUtil.getCurrentTimestamp().substring(0, 17));
            hm.put(FXTag.OrderQty, Integer.toString(totalQuantity));
            hm.put(FXTag.Price, Double.toString(lmtPrice));
            hm.put(FXTag.Currency, currency);
            FXPacket pkt = new FXPacket(FXType.PLACE_SINGLE_ORDER, hm);
            pkt.sendOut(out);
            System.out.println("bidOrderPkt sent out: " + pkt.toString());
        } catch (Exception e) {
            System.out.println("bidOrderPkt error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    public static String object2String(FXOrder order) {
        String objString = null;
        ByteArrayOutputStream baops = null;
        ObjectOutputStream oos = null;
        try {
            baops = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baops);
            oos.writeObject(order);
            byte[] bytes = baops.toByteArray();
            bytes = Base64.encodeBase64(bytes);
            objString = new String(bytes);
            oos.close();
            baops.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return objString;
    }

    public static FXOrder string2Object(String objString) {
        ObjectInputStream ois = null;
        FXOrder order = null;
        try {
            byte[] bytes = objString.getBytes();
            bytes = Base64.decodeBase64(bytes);
            ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
            order = (FXOrder) ois.readObject();
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return order;
    }

    @Override
    public String toString() {
        return "{symbol=" + this.symbol + ",currency=" + this.currency + ",orderId=" + orderId + ",price=" + this.lmtPrice + ",amount=" + this.totalQuantity + ",side=" + this.side + ",status=" + this.status + "}";
    }

    public static void main(String[] args) {
        try {
            System.out.println(FXOrder.string2Object(FXOrder.object2String(new FXOrder())).toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
