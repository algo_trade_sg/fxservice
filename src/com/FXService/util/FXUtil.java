/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.util;

import java.security.Key;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.Cipher;
import net.sf.json.JSONObject;
import org.apache.commons.codec.binary.Hex;

/**
 * @author qiangfu
 * @version 0.1
 * @since Mar 9, 2014
 */
public class FXUtil {

    private static String timeZoneCode = "GMT";//"Asia/Shanghai";

    public static double roundCeil(double input, double level, String format) {
        java.text.DecimalFormat myformat = new java.text.DecimalFormat("#" + format);
        double res = (int) Math.ceil(input / level) * level;
        return Double.valueOf(myformat.format(res));
    }

    public static double roundFloor(double input, double level, String format) {
        java.text.DecimalFormat myformat = new java.text.DecimalFormat("#" + format);
        double res = (int) Math.floor(input / level) * level;
        return Double.valueOf(myformat.format(res));
    }

    public static int calcCheckSum(byte[] bytes) {
        int sum = 0;
        for (int i = 0; i < bytes.length; i++) {
            sum += (int) bytes[i];
        }
        sum = sum % 256;
        return sum;
    }

    public static String getCurrentTimestamp() {
        TimeZone tz = TimeZone.getTimeZone(timeZoneCode);
        Date date = new Date();
        DateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss.SSS");
        sdf.setTimeZone(tz);
        return sdf.format(date);
    }

    public static String getCurrentTimestamp(String timeZoneCode, String dateFormat) {
        TimeZone tz = TimeZone.getTimeZone(timeZoneCode);
        Date date = new Date();
        DateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(tz);
        return sdf.format(date);
    }

    public static long getCurrentTimestampLong() {
        Date date = new Date();
        long ts = date.getTime();
        return ts;
    }

    public static long getCurrentTimestampLong(String timeZoneCode) {
        Date date = new Date();
        long ts = date.getTime();
        return ts;
    }

    public static String formatTimestamp(long ts) {
        TimeZone tz = TimeZone.getTimeZone(timeZoneCode);
        Date date = new Date(ts);
        DateFormat sdf = new SimpleDateFormat("yyyyMMdd-HH:mm:ss");
        sdf.setTimeZone(tz);
        return sdf.format(date);
    }

    public static String formatTimestamp(long ts, String timeZoneCode, String dateFormat) {
        TimeZone tz = TimeZone.getTimeZone(timeZoneCode);
        Date date = new Date(ts);
        DateFormat sdf = new SimpleDateFormat(dateFormat);
        sdf.setTimeZone(tz);
        return sdf.format(date);
    }

    public static double getPassedTime(String time) {//by hour
        double passedTime = 0.0;
        String dateFormat = "yyyyMMdd-HH:mm:ss";
        time = time.substring(0, dateFormat.length());
        try {
            Date date1 = new SimpleDateFormat(dateFormat).parse(time);
            Date date2 = new Date();
            passedTime = ((double) (date2.getTime() - date1.getTime())) / (1000 * 60 * 60);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return passedTime;
    }

    public static byte[] buildDesKey(byte[] keyByte) {
        byte[] byteTemp = new byte[8];
        for (int i = 0; i < byteTemp.length && i < keyByte.length; i++) {
            byteTemp[i] = keyByte[i];
        }
        Key key = new SecretKeySpec(byteTemp, "DES");
        return key.getEncoded();
    }

    public static byte[] encryptDES(byte[] encryptBytes, byte[] encryptKey) throws Exception {
        SecretKeySpec key = new SecretKeySpec(buildDesKey(encryptKey), "DES");
        Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] encryptedData = cipher.doFinal(encryptBytes);
        return encryptedData;
    }

    public static byte[] decryptDES(byte[] decryptBytes, byte[] decryptKey) throws Exception {
        SecretKeySpec key = new SecretKeySpec(buildDesKey(decryptKey), "DES");
        Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
        cipher.init(Cipher.DECRYPT_MODE, key);
//        byte decryptedData[] = cipher.doFinal(PBOCDESConvertUtil.hexStringToByte(decryptString));  
        byte decryptedData[] = cipher.doFinal(decryptBytes);
        return decryptedData;
    }

    public static byte[] bytesToHex(byte[] bytes) {
        return new String(Hex.encodeHex(bytes)).getBytes();
    }

    //TODO: this function is just for String:String type, otherwise change to Gson
    public static HashMap jsonStringToHashMap(String jsonStr) {
        HashMap hm = new HashMap();
        JSONObject jsonObj = JSONObject.fromObject(jsonStr);
        Iterator it = jsonObj.keys();
        String keyStr = "";
        String valStr = "";
        while (it.hasNext()) {
            keyStr = (String) it.next();
            valStr = (String) jsonObj.get(keyStr);
            hm.put(keyStr, valStr);
        }
        return hm;
    }

    public static void main(String[] args) throws Exception {
        long ts = getCurrentTimestampLong();
        System.out.println(ts);
        String tss = formatTimestamp(ts);
        System.out.println(tss);
    }
}
