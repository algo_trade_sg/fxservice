package com.FXService.market.processor;

import com.FXService.util.FXPacket;

public interface MarketProcessor {
    public void process(FXPacket pkt);
}
