package com.FXService.framework;

import java.math.BigDecimal;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class FXMarketDataCenter {
    private Map<String, BigDecimal> priceMap = new ConcurrentHashMap<>();
    private Map<String, Long> volumeMap = new ConcurrentHashMap<>();

    private static final BigDecimal ONE = new BigDecimal(1);

    public void update(String securityID, BigDecimal bidPrice, long bidVolume, BigDecimal askPrice, long askVolume) {
        priceMap.put(securityID, bidPrice);
        volumeMap.put(securityID, bidVolume);

        String reverseID = genReverseID(securityID);

        priceMap.put(reverseID, ONE.divide(askPrice, 10, BigDecimal.ROUND_HALF_UP));
        volumeMap.put(securityID, askVolume);
    }

    public BigDecimal getPrice(String securityID) {
        return priceMap.get(securityID);
    }

    public Long getVolume(String securityID) {
        return volumeMap.get(securityID);
    }

    private String genReverseID(String s) {
        StringBuilder sb = new StringBuilder();
        sb.substring(3, 6);
        return sb.toString();
    }

}
