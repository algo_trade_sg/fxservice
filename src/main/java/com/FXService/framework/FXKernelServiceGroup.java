package com.FXService.framework;

//import com.FXService.strategy.FXKernelArbitrage;
import org.apache.log4j.Logger;

public class FXKernelServiceGroup extends ThreadGroup {

    FXKernelServiceGroup() {
        super("FXKernelServiceGroup");
    }
    static final Logger logger = Logger.getLogger(FXKernelServiceGroup.class);
    public static FXServiceBase FXKernelBase = null;
    public static FXKernelArbitrage FXKernelArbitrage = null;

    public static void main(String[] args) {
        initFXKernelService();
    }

    private static void initFXKernelService() {
        try {
            FXKernelBase = new FXServiceBase();
            FXKernelBase.startSvr();
            
            FXKernelArbitrage = new FXKernelArbitrage();
            FXKernelArbitrage.startSvr();
        } catch (Exception e) {
            logger.error("init FXKernelBase error.", e);
        }
    }

    @Override
    public void uncaughtException(Thread t, Throwable e) {
        logger.error("uncaughtException from thread " + t + " : " + e);
    }
}
