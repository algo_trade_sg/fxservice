/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.framework;

import com.FXService.framework.Global;
import com.FXService.util.*;
import java.net.*;
import java.io.*;
import org.apache.log4j.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ArrayBlockingQueue;
import javax.net.ssl.SSLSocket;

/**
 * @author qiangfu
 * @version 0.1
 * @since Apr 1, 2014
 */
public class FXServiceBase implements Runnable {

    static final Logger logger = Logger.getLogger(FXServiceBase.class);
    public static int inPacketQueueSize = 128;
    public static int processorThreadsSize = 2;
    public static String heartBeatInterval = "5";
    public static String fixServerAddr = "fix-ate.lmaxtrader.com";
    public static String user = "shiroh880527";//testFX
    public static String pwd = "2Kgames01";//Xj$nbynsfwlb#C
    public static int fixServerPort = 443;
    Thread runThread = null;
    Thread heartThread = null;
    boolean isRun = false;
    boolean isLogin = false;
//    Socket socket = null;
    SSLSocket socket = null;
    public DataInputStream in = null;
    public DataOutputStream out = null;
    public long lastHeartBeateTS = 0;
    //processors
    private ArrayBlockingQueue<FXPacket> inPacketQueue = null;
    Thread[] processorThreads = null;

    static {
    }

    public FXServiceBase() {
        inPacketQueue = new ArrayBlockingQueue<FXPacket>(inPacketQueueSize);
    }

    public ArrayBlockingQueue<FXPacket> getInPacketQueue() {
        return this.inPacketQueue;
    }

    public void startSvr() {
        forceStop();
        startThreads();
        runThread = new Thread(this, "JServiceBase runThread");
        isRun = true;
        runThread.start();
    }

    public void forceStop() {
        try {
            if (runThread != null) {
                if (runThread.isAlive()) {
                    runThread.interrupt();
                    Thread.sleep(1000 * Integer.parseInt(heartBeatInterval));
                }
            }
            runThread = null;

            stopThreads();
        } catch (Exception e) {
            ;
        }
    }

    public boolean isLogin() {
        return this.isLogin;
    }

    public void stopSvr() {
        try {
            isRun = false;
            isLogin = false;
            forceStop();
            in.close();
            out.close();
            socket.close();
        } catch (Exception e) {
            //do nothing, todo: logging
        } finally {
            in = null;
            out = null;
            socket = null;
        }
        System.gc();
    }

    public int login(String username, String password) {
        try {
            try {
                if (socket != null) {
                    socket.close();
                    socket = null;
                }
            } catch (Exception e) {
            }
            socket = (SSLSocket) Global.socketFactory.createSocket(fixServerAddr, fixServerPort);
            socket.startHandshake();
            socket.setKeepAlive(true);
            in = new DataInputStream(socket.getInputStream());
            out = new DataOutputStream(socket.getOutputStream());
            FXCounter.resetCounter();
            System.out.println("Connect success! " + socket.toString());
        } catch (IOException e) {
            System.out.println("Connect failed!");
            return 0;
        }

        try {
            HashMap hm = new HashMap();
            hm.put(FXTag.EncryptMethod, "0");
            hm.put(FXTag.HeartBtInt, heartBeatInterval);
//            hm.put(FXTag.RawData, "X:" + username + ":" + password);
            hm.put(FXTag.Username, username);
            hm.put(FXTag.Password, password);
            hm.put(FXTag.ResetSeqNumFlag, "Y");
            FXPacket pkt = new FXPacket(FXType.LOGIN, hm);
            pkt.sendOut(out);
            //todo process other pkt
            System.out.println("loginPkt sent out: " + pkt.toString());
            FXPacket loginResultPkt = new FXPacket(in);
            System.out.println("loginResultPkt: " + loginResultPkt.toString());
            isLogin = true;
            return 1;
        } catch (Exception e) {
            System.out.println("login error: " + e.getMessage());
            e.printStackTrace();
            return 0;
        }
    }

    public void logout() {
        try {
            in.close();
            out.close();
            socket.close();
            isLogin = false;
        } catch (Exception e) {
            System.out.println("logout error: " + e.getMessage());
        } finally {
            in = null;
            out = null;
            socket = null;
        }
    }

    public void onLogin() {
        try {
        } catch (Exception e) {
            System.out.println("onlogin error: " + e.getMessage());
        }
    }

    public void onRun() {
        try {
        } catch (Exception e) {
            System.out.println("onlogin error: " + e.getMessage());
        }
    }

    public void run() {
        while (isRun) {
            int retry = 1;
            while ((login(user, pwd) == 0)) {
                System.out.println("login failed! Retry wait: " + retry);
                retry *= 2;
                if (retry > 512) {
                    retry = 512;
                }
                try {
                    Thread.sleep(1000 * 1 * retry);
                } catch (Exception e) {
                }
            }
            System.out.println("login successful!");
            if (heartThread != null && heartThread.isAlive()) {
                heartThread.stop();
            }
            heartThread = new Thread("heartThread") {

                @Override
                public void run() {
                    try {
                        lastHeartBeateTS = System.currentTimeMillis();
                        int requestAccountFrequency = 24;
                        int rafCounter = 0;
                        while (!Thread.interrupted()) {
                            if (System.currentTimeMillis() - lastHeartBeateTS
                                    > 3 * 1000 * Integer.parseInt(heartBeatInterval)) {
                                System.out.println("heartBeatPkt timeout!");
//                                int retry = 1;
//                                while ((login(user, pwd) == 0)) {
//                                    System.out.println("login failed! Retry wait: "+retry);
//                                    retry *= 2;
//                                    if(retry > 512)
//                                        retry = 512;
//                                    try {
//                                        Thread.sleep(1000 * 1 * retry);
//                                    } catch (Exception e) {
//                                    }
//                                }  
                            }
                            HashMap hm = new HashMap();
                            FXPacket heartBeatPkt = new FXPacket(FXType.HEARTBEAT, hm);
                            heartBeatPkt.sendOut(out);
                            System.out.println("heartBeatPkt sent out: " + heartBeatPkt.toString());
                            //request account infomation
                            rafCounter = (rafCounter + 1) % requestAccountFrequency;
                            Thread.sleep(1000 * Integer.parseInt(heartBeatInterval));

                            onRun();
                        }
                    } catch (Exception e) {
                        System.out.println("Exception on sending ping");
                    }
                }
            };
            heartThread.start();
            onLogin();
            try {
                while (isLogin) {
                    FXPacket pkt = new FXPacket(in);
                    this.inPacketQueue.offer(pkt);
                }
            } catch (Exception e) {
                System.out.println("Error occurs: " + e.getMessage());
                try {
                    in.close();
                    out.close();
                    socket.close();
                    isLogin = false;
                } catch (Exception ee) {
                } finally {
                    in = null;
                    out = null;
                    socket = null;
                }
            }
        }
    }

    private void startThreads() {
        processorThreads = new Thread[processorThreadsSize];
        for (int i = 0; i < processorThreadsSize; ++i) {
            processorThreads[i] = new FXPacketProcessor(this, "ProcessorThread-" + i);
            processorThreads[i].start();
            System.out.println("ProcessorThread-" + i + " started!");
        }
    }

    private void stopThreads() {
        for (int i = 0; i < processorThreadsSize; ++i) {
            if (processorThreads[i] != null) {
                processorThreads[i].interrupt();
                processorThreads[i] = null;
                System.out.println("ProcessorThread-" + i + " interupted!");
            }
        }
    }

    public static void main(String[] args) {
//        8=FIX.4.4|9=102|35=A|34=1|49=FIXtest1|52=20120924-14:05:44.817|56=LMXBDM|553=FIXtest1|554=Password1|98=0|108=30|141=Y|10=011|
//        Global.initSystemStatus();
//        JServiceBase jserviceBase = new JServiceBase();
//        jserviceBase.startSvr();
        System.out.println(System.getProperty("java.home"));
        FXServiceBase fxsb = new FXServiceBase();
        fxsb.startSvr();
    }
}
