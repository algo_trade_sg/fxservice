package com.FXService.framework;

import java.io.DataInputStream;
import java.io.IOException;

import javax.net.ssl.SSLSocket;

import com.FXService.util.FDataInputStream;
import com.FXService.util.FDataOutputStream;

public class FXMarketSocket {
    private static FXMarketSocket instance = new FXMarketSocket();

    private FDataInputStream fis;
    private FDataOutputStream fos;

    public FXMarketSocket getInstance() {
        return instance;
    }

    private SSLSocket createSSLSocket(String host, int port) throws IOException {
        SSLSocket socket = (SSLSocket) Global.socketFactory.createSocket(host, port);
        socket.startHandshake();
        socket.setKeepAlive(true);
        socket.setTcpNoDelay(true);

        fis = new FDataInputStream(socket.getInputStream());
        fos = new FDataOutputStream(socket.getOutputStream());
        return socket;
    }

    public FDataInputStream getInputStream() {
        return fis;
    }

    public FDataOutputStream getOutputStream() {
        return fos;
    }

    public void login(String user, String pwd) {
        
    }
}
