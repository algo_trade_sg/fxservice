package com.FXService.framework;

import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.log4j.Logger;

import com.FXService.common.FXThreadPoolFactory;
import com.FXService.market.processor.MarketProcessor;
import com.FXService.util.FXPacket;

public class FXMarketDispatcher {

    private Map<String, MarketProcessor> processors;
    private FXServiceBase jServiceBase;
    private Logger log = Logger.getLogger(FXMarketDispatcher.class);
    //@formatter:off
    private ThreadPoolExecutor exe = FXThreadPoolFactory.build("market-processor", 
                                     2, Runtime.getRuntime().availableProcessors(), 60, 10000);
    //@formatter:on
    public FXMarketDispatcher(FXServiceBase jServiceBase, int threadNumber) {
        this.jServiceBase = jServiceBase;
    }

    public void start() {
        exe.submit(new Runnable() {

            private void process(String type, FXPacket pkt) {
                MarketProcessor processor = processors.get(type);
                processor.process(pkt);
            }

            @Override
            public void run() {
                try {
                    FXPacket packet = jServiceBase.getInPacketQueue().take();
                    String type = packet.msgType;
                    process(type, packet);

                } catch (InterruptedException e) {
                    log.error("error occures in process threads");
                    log.error(e.getMessage());
                }
            }

        });
    }

}
