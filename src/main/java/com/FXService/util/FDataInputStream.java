package com.FXService.util;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.FXService.framework.Global;

public class FDataInputStream extends DataInputStream {
    
    public FDataInputStream(InputStream in) {
        super(in);
    }
    
    public String readField() throws IOException {
        StringBuilder sb = new StringBuilder();
        byte b;
        do{
            b = super.readByte();
            sb.append(b);
        }while(b != Global.SOH);
        return sb.toString();
    }

    public byte[] readFully(int len) throws IOException {
        byte[] data = new byte[len];
        super.readFully(data, 0, len);
        return data;
    }
}
