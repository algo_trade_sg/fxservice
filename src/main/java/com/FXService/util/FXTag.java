/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.util;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationFactory.ConfigurationBuilder;
import org.apache.commons.configuration.PropertiesConfiguration;

/**
 * @author qiangfu
 * @version 0.1
 * @since Apr 9, 2014
 */

public class FXTag {

    private static PropertiesConfiguration conf = FXConfig.getInstance().getConfig();

    public final static Set<String> groupTagSet = new HashSet<>();
    public final static Set<String> headerTagSet = new HashSet<>();
    public final static Set<String> tailerTagSet = new HashSet<>();
    public final static HashMap<String, Set<String>> groupFieldsMap = new HashMap<>();

    // for single tag use
    public final static String Account = "1";
    public final static String AvgPx = "6";
    public final static String BeginSeqNo = "7";
    public final static String BeginString = "8";
    public final static String BodyLength = "9";
    public final static String CheckSum = "10";
    public final static String ClOrdID = "11";
    public final static String CumQty = "14";
    public final static String Currency = "15";
    public final static String EndSeqNo = "16";
    public final static String ExecID = "17";
    public final static String ExecTransType = "20";
    public final static String HandlInst = "21";
    public final static String LastPx = "31";
    public final static String LastShares = "32";
    public final static String MsgSeqNum = "34";
    public final static String MsgType = "35";
    public final static String NewSeqNo = "36";
    public final static String OrderID = "37";
    public final static String OrderQty = "38";
    public final static String OrdStatus = "39";
    public final static String OrdType = "40";
    public final static String OrigClOrdID = "41";
    public final static String Price = "44";
    public final static String RefSeqNum = "45";
    public final static String SecurityID = "48";
    public final static String SenderCompID = "49";
    public final static String SendingTime = "52";
    public final static String Side = "54";
    public final static String Symbol = "55";
    public final static String TargetCompID = "56";
    public final static String Text = "58";
    public final static String TransactTime = "60";
    public final static String RawDataLength = "95";
    public final static String RawData = "96";
    public final static String EncryptMethod = "98";
    public final static String CxlRejReason = "102";
    public final static String OrdRejReason = "103";
    public final static String HeartBtInt = "108";
    public final static String TestReqID = "112";
    public final static String GapFillFlag = "123";
    public final static String ResetSeqNumFlag = "141";
    public final static String ExecType = "150";
    public final static String LeavesQty = "151";
    public final static String SecurityExchange = "207";
    public final static String RefTagID = "371";
    public final static String RefMsgType = "372";
    public final static String SessionRejectReason = "373";
    public final static String BusinessRejectReason = "380";
    public final static String CashMargin = "544";
    public final static String Username = "553";
    public final static String Password = "554";
    public final static String LongQty = "704";
    public final static String ShortQty = "705";
    public final static String PosAmt = "708";
    public final static String PosReqID = "710";
    public final static String PosMaintRptID = "721";
    public final static String PosReqType = "724";
    public final static String TotalNumPosReports = "727";
    public final static String PosReqResult = "728";
    public final static String LastRptRequested = "912";
    // for group tag use, local defined
    public final static String PosType = "703";
    public final static String PosType_SB = "703_SB";
    public final static String PosType_SAV = "703_SAV";
    public final static String PosType_SQ = "703_SQ";
    public final static String PosType_LB = "703_LB";
    public final static String PosType_SS = "703_SS";
    public final static String PosType_SF = "703_SF";
    public final static String PosType_SBQ = "703_SBQ";
    public final static String PosAmtType = "707";
    public final static String PosAmtType_FB = "707_FB";
    public final static String PosAmtType_FAV = "707_FAV";
    public final static String PosAmtType_MV = "707_MV";
    public final static String PosAmtType_F = "707_F";
    public final static String PosAmtType_SV = "707_SV";
    public final static String PosAmtType_FBF = "707_FBF";
    public final static String PosAmtType_BC = "707_BC";
    public final static String PosAmtType_SMV = "707_SMV";
    public final static String PosAmtType_IC = "707_IC";
    public final static String PosAmtType_PC = "707_PC";
    public final static String PosAmtType_BPL = "707_BPL";
    public final static List GroupKeys = Arrays.asList(new String[] { "703", "707" });
    public final static List GroupVals = Arrays.asList(new String[] { "704", "705", "708" });

    // lmax header extra
    public static final String PossDupFlag = "43";
    public static final String PossResend = "97";
    public static final String OrigSendingTime = "122";
    // Market data
    public static final String NoMDEntries = "268";
    public static final String MDEntryType = "269";

    public static void init() {
        String[] headerTags = conf.getStringArray("tag.headers");
        String[] tailerTags = conf.getStringArray("tag.tailers");
        String[] groupTags = conf.getStringArray("tag.groupTags");

        for (String s : headerTags) {
            headerTagSet.add(s);
        }
        for (String s : tailerTags) {
            tailerTagSet.add(s);
        }

        for (String s : groupTags) {
            groupTagSet.add(s);
            String[] fields = conf.getStringArray(String.format("tag.%s.fields", s));
            Set<String> set = new HashSet<>();
            for (String field : fields) {
                set.add(field);
            }
            groupFieldsMap.put(s, set);
        }
    }

}
