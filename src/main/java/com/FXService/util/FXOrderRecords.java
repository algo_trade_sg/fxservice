/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author qiangfu
 */
public class FXOrderRecords {

    HashMap<Integer, FXOrder> orders = null;
    private static String serializedOrdersHashKey = "FX:serialized:orders";

    public FXOrderRecords() {
        orders = new HashMap<Integer, FXOrder>();
        this.load();
    }

    //add order
    public void addOrder(FXOrder order) {
        orders.put(order.m_orderId, order);
        this.save(order);
    }

    //update order
    public void updateOrder(FXOrder order) {
        orders.put(order.m_orderId, order);
        this.save(order);
    }

    //remove order
    public void removeOrder(int orderId) {
        FXOrder order = orders.get(orderId);
        orders.remove(order.m_orderId);
        FXRedis.removeFromHashTable(serializedOrdersHashKey, "" + order.m_orderId);
    }
    
    //set order status
    public void setOrderStatus(int orderId, OrderStatus os) {
        FXOrder order = orders.get(orderId);
        order.m_status = os;
        orders.put(order.m_orderId, order);
        this.save(order);
    }

    //save
    public void save() {
        FXRedis.saveHashTableFromHashMap(serializedOrdersHashKey, this.orders2StringHashMap());
    }

    public void save(FXOrder order) {
        HashMap<String, String> hm = new HashMap<String, String>();
        hm.put("" + order.m_orderId, FXOrder.object2String(order));
        FXRedis.saveHashTableFromHashMap(serializedOrdersHashKey, hm);
    }

    //load
    public void load() {
        this.stringHashMap2Orders(FXRedis.loadHashMapFromHashTable(serializedOrdersHashKey));
        System.out.println("[FXOrderRecords] [load] orders="+this.orders.toString());
    }

    private HashMap<String, String> orders2StringHashMap() {
        HashMap<String, String> hm = new HashMap<String, String>();
        try {
            Iterator it = this.orders.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                String orderId = entry.getKey().toString();
                FXOrder order = (FXOrder) entry.getValue();
                String objString = FXOrder.object2String(order);
                hm.put(orderId, objString);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return hm;
    }

    private void stringHashMap2Orders(HashMap<String, String> hm) {
        try {
            this.orders = new HashMap<Integer, FXOrder>();
            Iterator it = hm.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                int orderId = Integer.parseInt(entry.getKey().toString());
                String objString = (String) entry.getValue();
                FXOrder order = FXOrder.string2Object(objString);
                this.orders.put(orderId, order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "{orders=" + this.orders.toString() + "}";
    }

    public static void main(String[] args) {
        try {
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
