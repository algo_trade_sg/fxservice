package com.FXService.util;

import java.io.FileInputStream;

import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.log4j.Logger;

public class FXConfig {
    private static Logger log = Logger.getLogger(FXConfig.class);
    private static FXConfig instance = new FXConfig();

    private static PropertiesConfiguration conf = new PropertiesConfiguration();

    public static void init() {
        try {
            conf.load(new FileInputStream("conf/system.properties"));
        } catch (Throwable t) {
            log.error("error occurs when loading configuration file");
            log.error(t.getMessage());
        }
    }
    public static FXConfig getInstance() {
        return instance;
    }

    public PropertiesConfiguration getConfig() {
        return conf;
    }

}
