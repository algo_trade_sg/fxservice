/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.util;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author qiangfu
 */
public class FXOrder {

    public String m_symbol;
    public String m_currency;

//    public FXOrder(FXOrder order) {
//        this.m_symbol = order.m_symbol;
//        this.m_currency = order.m_currency;
//        this.m_clientId = order.m_clientId;
//        this.m_orderId = order.m_orderId;
//        this.m_permId = order.m_permId;
//        this.m_lmtPrice = order.m_lmtPrice;
//        this.m_totalQuantity = order.m_totalQuantity;
//        this.m_action = order.m_action;
//        this.m_orderType = "LMT";
//        this.m_transmit = true;
//    }
//
//    public FXOrder(String symbol, String currency, String action) {
//        this.m_symbol = symbol;
//        this.m_currency = currency;
//        this.m_action = action;
//        this.m_orderType = "LMT";
//        this.m_transmit = true;
//    }
//
//    public FXOrder(String symbol, String currency, int clientId, int orderId, int amount, double price, String action) {
//        this.m_symbol = symbol;
//        this.m_currency = currency;
//        this.m_clientId = clientId;
//        this.m_orderId = orderId;
//        this.m_permId = orderId;
//        this.m_lmtPrice = price;
//        this.m_totalQuantity = amount;
//        this.m_action = action;
//        this.m_orderType = "LMT";
//        this.m_transmit = true;
//    }
//
//    public void setOrder(int clientId, int orderId, OrderStatus os) {
//        this.m_clientId = clientId;
//        this.m_orderId = orderId;
//        this.m_permId = orderId;
//        this.m_status = os;
//    }
//
//    public void setOrder(int clientId, int orderId, int amount, double price) {
//        this.m_clientId = clientId;
//        this.m_orderId = orderId;
//        this.m_permId = orderId;
//        this.m_lmtPrice = price;
//        this.m_totalQuantity = amount;
//    }

    public static String object2String(FXOrder order) {
        String objString = null;
        ByteArrayOutputStream baops = null;
        ObjectOutputStream oos = null;
        try {
            baops = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(baops);
            oos.writeObject(order);
            byte[] bytes = baops.toByteArray();
            bytes = Base64.encodeBase64(bytes);
            objString = new String(bytes);
            oos.close();
            baops.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return objString;
    }

    public static FXOrder string2Object(String objString) {
        ObjectInputStream ois = null;
        FXOrder order = null;
        try {
            byte[] bytes = objString.getBytes();
            bytes = Base64.decodeBase64(bytes);
            ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
            order = (FXOrder) ois.readObject();
            ois.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return order;
    }

//    @Override
//    public String toString() {
//        return "{symbol=" + this.m_symbol + ",currency=" + this.m_currency + ",orderId=" + m_orderId + ",price=" + this.m_lmtPrice + ",amount=" + this.m_totalQuantity + ",action=" + this.m_action + ",status=" + this.m_status + "}";
//    }

    public static void main(String[] args) {
        try {
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
