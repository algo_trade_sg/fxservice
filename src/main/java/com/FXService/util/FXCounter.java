/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.util;

/**
 * @author qiangfu
 * @version 0.1
 * @since Mar 9, 2014
 */

public class FXCounter {//will implement runnable, use redis to record
    
    private static int counter = 1;   
    
    public static void resetCounter(){
        counter = 1;
    }
     
    public static int setCounter(int newCounter){
        return counter = newCounter;
    }
     
    public static int getCounter(){
        return counter++;
    }
    
    public static void main(String[] args){
        
    }
}
