/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.FXService.common;

/**
 * @author qiangfu
 * @version 0.1
 * @since Mar 1, 2014
 */

public class StringHelper {
    public static String trimBegin(String stack, String needle){
        if(stack==null||stack.equals(""))return stack;
        if(needle==null || needle.equals(""))return stack;
        if(stack.startsWith(needle, 0)){
           return stack.substring(needle.length());
        }
        return stack;
    }

    private static String trimEnd(String stack, String needle){
        if(stack==null||stack.equals(""))return stack;
        if(needle==null || needle.equals(""))return stack;
        if(stack.endsWith(needle)){
           return stack.substring(0, stack.length() - needle.length());
        }
        return stack;
    }

    public static String shortenUrl(String url, int limit){
        if(url==null)return "";
        url = url.trim();
        url = trimBegin(url, "native:");
        //url = trimBegin(url, "http://");
        //url = trimBegin(url, "https://");
        url = trimEnd(url, "/");
        if(url.length()<limit)return url;
        int len = url.length();
        int nhead = (limit-3)/2;
        return String.format("%s...%s", url.substring(0,nhead), url.substring(len - (limit-nhead-3)));
    }

    public static String firstNChars(String str, int n){
        if(str==null)return "";
        return str.substring(0, Math.min(n, str.length()));
    }

    public static void main(String[] args){
        System.out.println(shortenUrl("http://www.jsonlint.com/", 16));
    }
}
