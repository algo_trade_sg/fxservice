package com.FXService.common;

import java.lang.Thread.UncaughtExceptionHandler;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

public class FXThreadPoolFactory {

    private static Logger log = Logger.getLogger(FXThreadPoolFactory.class);

    public static ThreadPoolExecutor build(final String name, final int coreSize, final int maxSize, final int keepAliveTime, final int queueSize) {
        ThreadFactory tf = new ThreadFactoryBuilder().setNameFormat("name-%d").setUncaughtExceptionHandler(new UncaughtExceptionHandler() {

            @Override
            public void uncaughtException(Thread t, Throwable e) {
                log.error("Uncaught error occurs in monitor-process thread" + e.getMessage(), e);
            }
        }).build();

        ThreadPoolExecutor exe = new ThreadPoolExecutor(coreSize, maxSize, keepAliveTime, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(queueSize), tf,
                new RejectedExecutionHandler() {

                    @Override
                    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                        // TODO Auto-generated method stub
                        log.error(name + "_queue is full. Task is rejected");
                    }
                });

        return exe;

    }
}
