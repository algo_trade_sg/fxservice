/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.FXService.common;

/**
 * @author qiangfu
 * @version 0.1
 * @since Mar 1, 2014
 */

public class TimestampEntry<T> {
    T obj;
    long expTime;

    public TimestampEntry(T obj) {
        this.obj = obj;
        expTime = System.currentTimeMillis();
    }

    public TimestampEntry(T obj, long expTime) {
        this.obj = obj;
        this.expTime = expTime;
    }

    /**
     *
     * @param expire ms
     * @return
     */
    public T getAliveObj(long expire){
        if(System.currentTimeMillis() - this.expTime <= expire ){
            return this.obj;
        }
        return null;
    }

    public boolean isExpired(long expire){
        return (System.currentTimeMillis() - this.expTime > expire );
    }

    public T getObj(){
        return this.obj;
    }
}
