/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.FXService.common;

import com.FXService.common.StopWatch;
import java.io.IOException;
import java.net.InetSocketAddress;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;

/**
 * @author qiangfu
 * @version 0.1
 * @since Mar 1, 2014
 */
public class HttpUtil {

    private static Logger log = Logger.getLogger(HttpUtil.class);
    public static int DEFAULT_GET_TIMEOUT = 10000;
    public static int DEFAULT_POST_TIMEOUT = 10000;

    public static String httpGet(String url) {
        return httpGet(url, DEFAULT_GET_TIMEOUT);
    }

    public static String httpGet(String url, int timeout) {
        HttpClient client = new HttpClient();

        // Create a method instance.
        GetMethod method = new GetMethod(url);

        method.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");

        // Provide custom retry handler is necessary
        method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
                new DefaultHttpMethodRetryHandler(3, false));

        String res = "";
        StopWatch stopWatch = new StopWatch();
        try {
            // Execute the method.
            client.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);
            client.getHttpConnectionManager().getParams().setSoTimeout(timeout);
            int statusCode = client.executeMethod(method);

            if (statusCode != HttpStatus.SC_OK) {
                throw new RuntimeException("get Method failed: " + method.getStatusLine() + "\n"
                        + "    for url: " + url);
            }

            // Read the response body.
            res = new String(method.getResponseBody(), "UTF-8");
        } catch (Exception e) {
            log.error(String.format("httpGet url=[%s], err=[%s]", url, e));
        } finally {
            // Release the connection.
            method.releaseConnection();
        }
        log.debug(String.format("HttpUtil.GET:%s, %s, resp80=%s", url, stopWatch.toString(), res));
        return res;
    }

    public static int httpPost(PostMethod filePost, int timeout) throws IOException {
        HttpClient client = new HttpClient();
        client.getHttpConnectionManager().getParams().setConnectionTimeout(timeout);
        client.getHttpConnectionManager().getParams().setSoTimeout(timeout);

        return client.executeMethod(filePost);
    }

    public static InetSocketAddress createSocketAddr(String target, int defaultPort) {
        int colonIndex = target.indexOf(':');
        if (colonIndex < 0 && defaultPort == -1) {
            throw new RuntimeException("Not a host:port pair: " + target);
        }
        String hostname;
        int port = -1;
        if (!target.contains("/")) {
            if (colonIndex == -1) {
                hostname = target;
            } else {
                hostname = target.substring(0, colonIndex);
                port = Integer.parseInt(target.substring(colonIndex + 1));
            }
        } else {
            throw new RuntimeException("Not a host:port pair: " + target);
        }
        if (port == -1) {
            port = defaultPort;
        }

        return new InetSocketAddress(hostname, port);
    }

    public static String intToIp(int i) {
        return ((i) & 0xFF) + "."
                + ((i >> 8) & 0xFF) + "."
                + ((i >> 16) & 0xFF) + "."
                + (i >> 24 & 0xFF);
    }

    public static int ipToInt(String addr) {
        String[] addrArray = addr.split("\\.");
        long num = 0;
        for (int i = 0; i < addrArray.length; i++) {
            int power = i;

            num += ((Integer.parseInt(addrArray[i]) % 256 * Math.pow(256, power)));
        }
        return new Long(num).intValue();
    }

    public static byte[] intToByteArray(int value) {
        return new byte[]{
                    (byte) (value >> 24),
                    (byte) (value >> 16 & 0xff),
                    (byte) (value >> 8 & 0xff),
                    (byte) (value & 0xff)};
    }
}
