package sg.trade.algo.lmax_fix;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import com.FXService.util.FXConfig;
import com.FXService.util.FXPacket;
import com.FXService.util.FXTag;
import com.google.common.base.Joiner;

public class PacketTest {

    private static final byte SOH = 0X01;

    public static void main(String args[]) throws Exception {
//        decodeTest();
//        encodeTest();
        decodePacket();
    }

    public static String[] decodeTest() throws IOException {
        FileInputStream fis = new FileInputStream("input.txt");
        byte[] input = IOUtils.toByteArray(fis);

        String s = new String(input);
        String sarray[] = StringUtils.split(s, (char) SOH);
        for (String s1 : sarray) {
            System.out.println(s1);
        }
        return sarray;
    }

    public static void encodeTest() throws IOException {

        String[] sample = decodeTest();
        String output = Joiner.on((char) SOH).join(sample);
        System.out.println(output);
//        byte[] bytes = output.getBytes();
//        for (int i = 0; i < bytes.length; ++i) {
//            if (bytes[i] == SOH) {
//                System.out.println(i);
//            }
//        }

    }

    public static void decodePacket() throws Exception {
        FileInputStream fis = new FileInputStream("input.txt");
        FileInputStream fis2 = new FileInputStream("input2.txt");
        FileInputStream fis3 = new FileInputStream("input.txt");
//        byte[] input = IOUtils.toByteArray(fis);
//        byte[] input2 = IOUtils.toByteArray(fis2);
        FXConfig.init();
        FXTag.init();
    
        FXPacket pkt = new FXPacket(new DataInputStream(new BufferedInputStream(fis)));

        FXPacket pkt2 = new FXPacket(new DataInputStream(new BufferedInputStream(fis2)));
        
        FXPacket pkt3 = new FXPacket(new DataInputStream(new BufferedInputStream(fis3)));
        
        pkt.toString();
        pkt2.toString();

    }
}
